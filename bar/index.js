var data = [
  ["Iceland",14.1],
  ["Egypt",30.4],
  ["Syria",27.6],
  ["Malaysia",17.5],
  ["Japan",8.3],
  ["USA",12.7],
  ["Taiwan",8.5],
  ["India",21.8],
  ["Germany",8.1]
];

path = d3.select("svg").selectAll("path").data(data).enter().append('path');
path.attr("d", function(d,i) {
  var x1 = 100, x2 = 100 + d[1] * 22;
  var y1 = i * 40, y2 = i * 40 + 30;
  return (
    "M" + x1 + " " + y1 +
    "L" + x2 + " " + y1 +
    "L" + x2 + " " + y2 +
    "L" + x1 + " " + y2 + "Z"
  );
});


var color = d3.scale.category20();
path.attr("fill", function(d,i) { return color(d[0]); });
